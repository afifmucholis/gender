<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gap extends Model
{
    protected $fillable = [
                            'user_id', 
    						'project_id', 
    						'langkah1', 
    						'langkah2', 
    						'langkah3', 
    						'langkah4', 
    						'langkah5', 
    						'langkah6', 
    						'langkah7', 
    						'langkah8', 
    						'langkah9', 
    						'konsultan_id', 
    						'catatan', 
    						'locked'];
    public function usernya() { 
    	return $this->hasMany('App\User', 'user_id');
    }

    public function konsultan() { 
    	return $this->hasMany('App\User', 'konsultan_id');
    }

}
