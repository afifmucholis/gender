<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gbs extends Model
{
    protected $fillable = [
                            'user_id', 
    						'project_id', 
    						'program', 
    						'kegiatan', 
    						'output', 
    						'tujuan', 
    						'analisa_situasi', 
    						'rencana_aksi', 
    						'alokasi_anggaran', 
    						'dampak', 
    						'konsultan_id', 
    						'catatan', 
    						'locked'];
    public function usernya() { 
    	return $this->hasMany('App\User', 'user_id');
    }

    public function konsultan() { 
    	return $this->hasMany('App\User', 'konsultan_id');
    }
}
