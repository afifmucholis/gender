<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Gap;
use App\Project;

class GapsController extends Controller
{

    public function __construct() { 
        $this->middleware('auth'); 
        $this->middleware('role:user'); 
    } 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $gaps = Project::where('projects.user_id', \Auth::user()->id)
                    ->join('gaps', 'projects.id', '=', 'gaps.project_id')
                    ->get();
        return view('gap', compact('gaps'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $id = $request->session()->get('id');
        return view('cgap', compact('id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, 
            [ 'project_id' => 'required',
              'langkah1' => 'required|string',
              'langkah2' => 'required|string', 
              'langkah3' => 'required|string', 
              'langkah4' => 'required|string', 
              'langkah5' => 'required|string', 
              'langkah6' => 'required|string', 
              'langkah7' => 'required|string', 
              'langkah8' => 'required|string', 
              'langkah9' => 'required|string']);

            $request->request->add(['catatan' => 'Masih dalam proses pengecekan',
                                    'user_id' => \Auth::user()->id]);
            $gap = Gap::create($request->all()); 
            $id = $gap->project_id;
            return redirect()->route('gbss.create')->with('id', $id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $gap = Gap::findOrFail($id); 
        $this->validate($request, 
                    [ 'langkah1' => 'required|string',
                      'langkah2' => 'required|string', 
                      'langkah3' => 'required|string', 
                      'langkah4' => 'required|string', 
                      'langkah5' => 'required|string', 
                      'langkah6' => 'required|string', 
                      'langkah7' => 'required|string', 
                      'langkah8' => 'required|string', 
                      'langkah9' => 'required|string']);
        $gap->update($request->all()); 
        return redirect()->route('gaps.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
