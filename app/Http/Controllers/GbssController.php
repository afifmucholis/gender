<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Gbs;
use App\Project;

class GbssController extends Controller
{

    public function __construct() { 
        $this->middleware('auth'); 
        $this->middleware('role:user'); 
    } 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gbss = Project::where('projects.user_id', \Auth::user()->id)
                    ->join('gbs', 'projects.id', '=', 'gbs.project_id')
                    ->get();
        return view('gbs', compact('gbss'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $id = $request->session()->get('id');
        return view('cgbs', compact('id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $this->validate($request, 
        //     [ 'project_id' => 'required',
        //       'program' => 'required',
        //       'kegiatan' => 'required', 
        //       'output' => 'required', 
        //       'tujuan' => 'required', 
        //       'analisa_situasi' => 'required', 
        //       'rencana_aksi' => 'required', 
        //       'alokasi_anggran' => 'required', 
        //       'dampak' => 'required']);
        //     var_dump($request->all());
        //     exit();

            $request->request->add(['user_id' => \Auth::user()->id,
                                    'catatan' => 'Masih dalam proses pengecekan']);
            $gbs = Gbs::create($request->all());
            $id = $gbs->project_id;
            return redirect()->route('tors.create')->with('id', $id); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $gbs = Gbs::findOrFail($id);
        $this->validate($request, 
            [ 'program' => 'required|string',
              'kegiatan' => 'required|string', 
              'output' => 'required|string', 
              'tujuan' => 'required|string', 
              'analisa_situasi' => 'required|string', 
              'alokasi_anggran' => 'required|string', 
              'dampak' => 'required|string']);
        $gbs->update($request->all()); 
        return redirect()->route('gbss.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
