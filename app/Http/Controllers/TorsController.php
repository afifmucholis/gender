<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tor;
use App\Project;

class TorsController extends Controller
{
    public function __construct() { 
        $this->middleware('auth'); 
        $this->middleware('role:user'); 
    } 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tors = Project::where('projects.user_id', \Auth::user()->id)
                    ->join('tors', 'projects.id', '=', 'tors.project_id')
                    ->get();
        return view('tor', compact('tors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $id = $request->session()->get('id');
        return view('ctor', compact('id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $this->validate($request, 
        //     [ 'user_id' => 'required|string', 
        //         'komponen' => 'required|string', 
        //         'opd' => 'required|string', 
        //         'program' => 'required|string', 
        //         'kegiatan' => 'required|string', 
        //         'indikator_kinerja' => 'required|string', 
        //         'latar_belakang' => 'required|string', 
        //         'gambaran_umum' => 'required|string', 
        //         'penerima_manfaat' => 'required|string', 
        //         'maksud' => 'required|string', 
        //         'cara_pelaksanaan' => 'required|string', 
        //         'lokasi' => 'required|string', 
        //         'pelaksana' => 'required|string', 
        //         'batas' => 'required|string', 
        //         'jadwal' => 'required|string', 
        //         'biaya' => 'required']);

        $request->request->add(['user_id' => \Auth::user()->id,
                                    'catatan' => 'Masih dalam proses pengecekan']);
        Tor::create($request->all()); 
        return redirect()->route('project.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tor = Tor::findOrFail($id);
        $this->validate($request, 
            [ 'user_id' => 'required|string', 
                'komponen' => 'required|string', 
                'opd' => 'required|string', 
                'program' => 'required|string', 
                'kegiatan' => 'required|string', 
                'indikator_kinerja' => 'required|string', 
                'latar_belakang' => 'required|string', 
                'gambaran_umum' => 'required|string', 
                'penerima_manfaat' => 'required|string', 
                'maksud' => 'required|string', 
                'cara_pelaksanaan' => 'required|string', 
                'lokasi' => 'required|string', 
                'pelaksana' => 'required|string', 
                'batas' => 'required|string', 
                'jadwal' => 'required|string', 
                'biaya' => 'required']);
        $tor->update($request->all()); 
        return redirect()->route('tors.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
