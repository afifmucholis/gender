<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = [
    						'judul',
    						'deskripsi',
    						'tahun',
    						'locked',
    						'user_id'];
     public function p_gap() { 
    	return $this->hasMany('App\Gap');
    }
}
