<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tor extends Model
{
    protected $fillable = [
                            'user_id', 
    						'project_id', 
    						'komponen', 
    						'opd', 
    						'program', 
    						'kegiatan', 
    						'indikator_kinerja', 
    						'latar_belakang', 
    						'gambaran_umum', 
    						'penerima_manfaat', 
    						'maksud', 
    						'cara_pelaksanaan', 
    						'lokasi', 
    						'pelaksana', 
    						'batas', 
    						'jadwal', 
    						'biaya', 
    						'konsultan_id', 
    						'catatan', 
    						'locked'];
    public function usernya() { 
    	return $this->hasMany('App\User', 'user_id');
    }

    public function konsultan() { 
    	return $this->hasMany('App\User', 'konsultan_id');
    }
}
