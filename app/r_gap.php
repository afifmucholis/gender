<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class r_gap extends Model
{
    protected $fillable = ['tahun', 'nama', 'deskripsi', 'no'];

    public function childs() { 
    	return $this->hasMany('App\tb_gap', 'r_gap_id');
    }

}
