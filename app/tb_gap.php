<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tb_gap extends Model
{
    protected $fillable = ['r_gap_id', 'user_id', 'isi', 'konsultan_id'];

    public function gaps() { 
    	return $this->hasMany('App\tb_gap', 'r_gap_id');
    }

    public function user() { 
    	return $this->hasMany('App\User', 'user_id');
    }

    public function konsultan() { 
    	return $this->hasMany('App\User', 'konsultan_id');
    }
}
