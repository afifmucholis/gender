<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRGapsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('r_gaps', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tahun'); 
            $table->string('nama'); 
            $table->text('deskripsi');
            $table->integer('no'); 
            $table->timestamps();
        });

        Schema::create('tb_gaps', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('r_gap_id')->unsigned();  
            $table->integer('user_id')->unsigned(); 
            $table->text('isi'); 
            $table->integer('konsultan_id')->unsigned(); 
            $table->text('catatan'); 
            $table->foreign('r_gap_id')->references('id')->on('r_gaps'); 
            $table->foreign('user_id')->references('id')->on('users'); 
            $table->foreign('konsultan_id')->references('id')->on('users'); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('r_gaps');
        Schema::dropIfExists('tb_gaps');
    }
}
