<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUsersColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function($table) {
            $table->string('nip');
            $table->string('jabatan');
            $table->string('hp');
            $table->string('opd');
            $table->string('bidang');
            $table->string('sub_bidang');
            $table->string('email_lembaga');
            $table->string('telp_kantor');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function($table) {
            $table->dropColumn('nip');
            $table->dropColumn('jabatan');
            $table->dropColumn('hp');
            $table->dropColumn('opd');
            $table->dropColumn('bidang');
            $table->dropColumn('sub_bidang');
            $table->dropColumn('email_lembaga');
            $table->dropColumn('telp_kantor');
        });
    }
}
