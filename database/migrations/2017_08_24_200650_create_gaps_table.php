<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGapsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gaps', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned(); 
            $table->text('langkah1');
            $table->text('langkah2');
            $table->text('langkah3');
            $table->text('langkah4');
            $table->text('langkah5');
            $table->text('langkah6');
            $table->text('langkah7');
            $table->text('langkah8');
            $table->text('langkah9');
            $table->integer('konsultan_id')->nullable(); 
            $table->text('catatan')->nullable();
            $table->boolean('locked')->default('0');
            $table->foreign('user_id')->references('id')->on('users'); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gaps');
    }
}
