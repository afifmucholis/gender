<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGbsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gbs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned(); 
             $table->text('program');
             $table->text('kegiatan');
             $table->text('output');
             $table->text('tujuan');
             $table->text('analisa_situasi');
             $table->text('rencana_aksi');
             $table->text('alokasi_anggaran');
             $table->text('dampak');
             $table->integer('konsultan_id')->nullable(); 
            $table->text('catatan')->nullable();
            $table->boolean('locked')->default('0');
            $table->foreign('user_id')->references('id')->on('users'); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gbs');
    }
}
