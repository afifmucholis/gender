<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tors', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned(); 
             $table->text('komponen');
             $table->text('opd');
             $table->text('program');
             $table->text('kegiatan');
             $table->text('indikator_kinerja');
             $table->text('latar_belakang');
             $table->text('gambaran_umum');
             $table->text('penerima_manfaat');
             $table->text('maksud');
             $table->text('cara_pelaksanaan');
             $table->text('lokasi');
             $table->text('pelaksana');
             $table->text('batas');
             $table->text('jadwal');
             $table->text('biaya');
             $table->integer('konsultan_id')->nullable(); 
            $table->text('catatan')->nullable();
            $table->boolean('locked')->default('0');
            $table->foreign('user_id')->references('id')->on('users'); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tors');
    }
}
