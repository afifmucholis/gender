<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::create([ 'name' => 'Admin', 'email' => 'admin@pprg.com', 'password' => bcrypt('secret'), 'role' => 'admin' ]);
        App\User::create([ 'name' => 'Kunsultan', 'email' => 'konsultan@pprg.com', 'password' => bcrypt('secret'), 'role' => 'konsultan' ]);
    }
}
