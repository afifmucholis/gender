
{!! Form::label('langkah1', 'Langkah 1') !!} <!-- langkah 1 -->
<div class="form-group row {!! $errors->has('langkah1') ? 'has-error' : '' !!}"> 
	{!! Form::label('langkah1', 'Kebijakan/Program/Kegiatan', ['class'=>'col-xs-12 col-sm-3 control-label no-padding-right']) !!} 
	<div class="col-xs-12 col-sm-5">
		<span class="block input-icon input-icon-right">
			{!! Form::textarea('langkah1', null, ['class'=>'form-control', 'rows'=>'3']) !!} 
		</span>
	</div>
	{!! $errors->first('langkah1', '<p class="help-block col-xs-12 col-sm-reset inline">:message</p>') !!} 
</div> <!-- end langkah 1 -->

{!! Form::label('langkah2', 'Langkah 2') !!} <!-- langkah 2 -->
<div class="form-group row {!! $errors->has('langkah2') ? 'has-error' : '' !!}"> 
	{!! Form::label('langkah2', 'Kebijakan/Program/Kegiatan', ['class'=>'col-xs-12 col-sm-3 control-label no-padding-right']) !!} 
	<div class="col-xs-12 col-sm-5">
		<span class="block input-icon input-icon-right">
			{!! Form::textarea('langkah2', null, ['class'=>'form-control', 'rows'=>'3']) !!} 
		</span>
	</div>
	{!! $errors->first('langkah2', '<p class="help-block col-xs-12 col-sm-reset inline">:message</p>') !!} 
</div> <!-- end langkah 2 -->

{!! Form::label('langkah3', 'Langkah 3') !!} <!-- langkah 3 -->
<div class="form-group row {!! $errors->has('langkah3') ? 'has-error' : '' !!}"> 
	{!! Form::label('langkah3', 'Kebijakan/Program/Kegiatan', ['class'=>'col-xs-12 col-sm-3 control-label no-padding-right']) !!} 
	<div class="col-xs-12 col-sm-5">
		<span class="block input-icon input-icon-right">
			{!! Form::textarea('langkah3', null, ['class'=>'form-control', 'rows'=>'3']) !!} 
		</span>
	</div>
	{!! $errors->first('langkah3', '<p class="help-block col-xs-12 col-sm-reset inline">:message</p>') !!} 
</div> <!-- end langkah 3 -->

{!! Form::label('langkah4', 'Langkah 4') !!} <!-- langkah 4 -->
<div class="form-group row {!! $errors->has('langkah4') ? 'has-error' : '' !!}"> 
{!! Form::label('langkah4', 'Kebijakan/Program/Kegiatan', ['class'=>'col-xs-12 col-sm-3 control-label no-padding-right']) !!} 
	<div class="col-xs-12 col-sm-5">
		<span class="block input-icon input-icon-right">
			{!! Form::textarea('langkah4', null, ['class'=>'form-control', 'rows'=>'3']) !!} 
		</span>
	</div>
	{!! $errors->first('langkah4', '<p class="help-block col-xs-12 col-sm-reset inline">:message</p>') !!} 
</div> <!-- end langkah 4 -->

{!! Form::label('langkah5', 'Langkah 5') !!} <!-- langkah 5 -->
<div class="form-group row {!! $errors->has('langkah5') ? 'has-error' : '' !!}"> 
	{!! Form::label('langkah5', 'Kebijakan/Program/Kegiatan', ['class'=>'col-xs-12 col-sm-3 control-label no-padding-right']) !!} 
	<div class="col-xs-12 col-sm-5">
		<span class="block input-icon input-icon-right">
			{!! Form::textarea('langkah5', null, ['class'=>'form-control', 'rows'=>'3']) !!} 
		</span>
	</div>
	{!! $errors->first('langkah5', '<p class="help-block col-xs-12 col-sm-reset inline">:message</p>') !!} 
</div> <!-- end langkah 5 -->

{!! Form::label('langkah6', 'Langkah 6') !!} <!-- langkah 1 -->
<div class="form-group row {!! $errors->has('langkah6') ? 'has-error' : '' !!}"> 
	{!! Form::label('langkah6', 'Kebijakan/Program/Kegiatan', ['class'=>'col-xs-12 col-sm-3 control-label no-padding-right']) !!} 
	<div class="col-xs-12 col-sm-5">
		<span class="block input-icon input-icon-right">
			{!! Form::textarea('langkah6', null, ['class'=>'form-control', 'rows'=>'3']) !!} 
		</span>
	</div>
	{!! $errors->first('langkah6', '<p class="help-block col-xs-12 col-sm-reset inline">:message</p>') !!} 
</div> <!-- end langkah 6 -->

{!! Form::label('langkah7', 'Langkah 7') !!} <!-- langkah 1 -->
<div class="form-group row {!! $errors->has('langkah7') ? 'has-error' : '' !!}"> 
	{!! Form::label('langkah7', 'Kebijakan/Program/Kegiatan', ['class'=>'col-xs-12 col-sm-3 control-label no-padding-right']) !!} 
	<div class="col-xs-12 col-sm-5">
		<span class="block input-icon input-icon-right">
			{!! Form::textarea('langkah7', null, ['class'=>'form-control', 'rows'=>'3']) !!} 
		</span>
	</div>
	{!! $errors->first('langkah7', '<p class="help-block col-xs-12 col-sm-reset inline">:message</p>') !!} 
</div> <!-- end langkah 7 -->

{!! Form::label('langkah8', 'Langkah 8') !!} <!-- langkah 1 -->
<div class="form-group row {!! $errors->has('langkah8') ? 'has-error' : '' !!}"> 
	{!! Form::label('langkah8', 'Kebijakan/Program/Kegiatan', ['class'=>'col-xs-12 col-sm-3 control-label no-padding-right']) !!} 
	<div class="col-xs-12 col-sm-5">
		<span class="block input-icon input-icon-right">
			{!! Form::textarea('langkah8', null, ['class'=>'form-control', 'rows'=>'3']) !!} 
		</span>
	</div>
	{!! $errors->first('langkah8', '<p class="help-block col-xs-12 col-sm-reset inline">:message</p>') !!} 
</div> <!-- end langkah 8 -->

{!! Form::label('langkah9', 'Langkah 9') !!} <!-- langkah 9 -->
<div class="form-group row {!! $errors->has('langkah9') ? 'has-error' : '' !!}"> 
	{!! Form::label('langkah9', 'Kebijakan/Program/Kegiatan', ['class'=>'col-xs-12 col-sm-3 control-label no-padding-right']) !!} 
	<div class="col-xs-12 col-sm-5">
		<span class="block input-icon input-icon-right">
			{!! Form::textarea('langkah9', null, ['class'=>'form-control', 'rows'=>'3']) !!} 
		</span>
	</div>
	{!! $errors->first('langkah9', '<p class="help-block col-xs-12 col-sm-reset inline">:message</p>') !!} 
</div> <!-- end langkah 9 -->
