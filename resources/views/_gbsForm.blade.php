<div class="form-group row {!! $errors->has('program') ? 'has-error' : '' !!}"> 
	{!! Form::label('program', 'Program', ['class'=>'col-xs-12 col-sm-3 control-label no-padding-right']) !!} 
	<div class="col-xs-12 col-sm-5">
		<span class="block input-icon input-icon-right">
			{!! Form::textarea('program', null, ['class'=>'form-control', 'rows'=>'3']) !!} 
		</span>
	</div>
	{!! $errors->first('program', '<div class="help-block col-xs-12 col-sm-reset inline">:message</div>') !!} 
</div> <!-- end program -->

<div class="form-group row {!! $errors->has('kegiatan') ? 'has-error' : '' !!}"> 
	{!! Form::label('kegiatan', 'Kegiatan', ['class'=>'col-xs-12 col-sm-3 control-label no-padding-right']) !!} 
	<div class="col-xs-12 col-sm-5">
		<span class="block input-icon input-icon-right">
			{!! Form::textarea('kegiatan', null, ['class'=>'form-control', 'rows'=>'3']) !!} 
		</span>
	</div>
	{!! $errors->first('kegiatan', '<div class="help-block col-xs-12 col-sm-reset inline">:message</div>') !!} 
</div> <!-- end kegiatan -->

<div class="form-group row {!! $errors->has('output') ? 'has-error' : '' !!}"> 
	{!! Form::label('output', 'Output', ['class'=>'col-xs-12 col-sm-3 control-label no-padding-right']) !!} 
	<div class="col-xs-12 col-sm-5">
		<span class="block input-icon input-icon-right">
			{!! Form::textarea('output', null, ['class'=>'form-control', 'rows'=>'3']) !!} 
		</span>
	</div>
	{!! $errors->first('output', '<didiv class="help-block col-xs-12 col-sm-reset inline">:message</div>') !!} 
</div> <!-- end output-->

<div class="form-group row {!! $errors->has('tujuan') ? 'has-error' : '' !!}"> 
	{!! Form::label('tujuan', 'Tujuan', ['class'=>'col-xs-12 col-sm-3 control-label no-padding-right']) !!} 
	<div class="col-xs-12 col-sm-5">
		<span class="block input-icon input-icon-right">
			{!! Form::textarea('tujuan', null, ['class'=>'form-control', 'rows'=>'3']) !!} 
		</span>
	</div>
	{!! $errors->first('tujuan', '<div class="help-block col-xs-12 col-sm-reset inline">:message</div>') !!} 
</div> <!-- end tujuan -->

<div class="form-group row {!! $errors->has('analisa_situasi') ? 'has-error' : '' !!}"> 
	{!! Form::label('analisa_situasi', 'Analisa Situasi', ['class'=>'col-xs-12 col-sm-3 control-label no-padding-right']) !!} 
	<div class="col-xs-12 col-sm-5">
		<span class="block input-icon input-icon-right">
			{!! Form::textarea('analisa_situasi', null, ['class'=>'form-control', 'rows'=>'3']) !!} 
		</span>
	</div>
	{!! $errors->first('analisa_situasi', '<div class="help-block col-xs-12 col-sm-reset inline">:message</div>') !!} 
</div> <!-- end analisa_situasi -->

<div class="form-group row {!! $errors->has('rencana_aksi') ? 'has-error' : '' !!}"> 
	{!! Form::label('rencana_aksi', 'Rencana Aksi', ['class'=>'col-xs-12 col-sm-3 control-label no-padding-right']) !!} 
	<div class="col-xs-12 col-sm-5">
		<span class="block input-icon input-icon-right">
			{!! Form::textarea('rencana_aksi', null, ['class'=>'form-control', 'rows'=>'3']) !!} 
		</span>
	</div>
	{!! $errors->first('rencana_aksi', '<div class="help-block col-xs-12 col-sm-reset inline">:message</div>') !!} 
</div> <!-- end rencana_aksi -->

<div class="form-group row {!! $errors->has('alokasi_anggaran') ? 'has-error' : '' !!}"> 
	{!! Form::label('alokasi_anggaran', 'Alokasi Anggaran', ['class'=>'col-xs-12 col-sm-3 control-label no-padding-right']) !!} 
	<div class="col-xs-12 col-sm-5">
		<span class="block input-icon input-icon-right">
			{!! Form::textarea('alokasi_anggaran', null, ['class'=>'form-control', 'rows'=>'3']) !!} 
		</span>
	</div>
	{!! $errors->first('alokasi_anggaran', '<div class="help-block col-xs-12 col-sm-reset inline">:message</div>') !!} 
</div> <!-- end alokasi_anggaran -->

<div class="form-group row {!! $errors->has('dampak') ? 'has-error' : '' !!}"> 
	{!! Form::label('dampak', 'Dampak', ['class'=>'col-xs-12 col-sm-3 control-label no-padding-right']) !!} 
	<div class="col-xs-12 col-sm-5">
		<span class="block input-icon input-icon-right">
			{!! Form::textarea('dampak', null, ['class'=>'form-control', 'rows'=>'3']) !!} 
		</span>
	</div>
	{!! $errors->first('dampak', '<div class="help-block col-xs-12 col-sm-reset inline">:message</div>') !!} 
</div> <!-- end dampak -->
