<div class="form-group {!! $errors->has('tahun') ? 'has-error' : '' !!}">
	{!! Form::label('tahun', 'Tahun', ['class'=>'col-xs-12 col-sm-3 control-label no-padding-right']) !!} 
	<div class="col-xs-12 col-sm-5">
		<span class="block input-icon input-icon-right">
			{!! Form::number('tahun', null, ['class'=>'width-100']) !!} 
		</span>
	</div>
	{!! $errors->first('tahun', '<div class="help-block col-xs-12 col-sm-reset inline">:message</div>') !!} 
</div> <!-- tahun -->

<div class="form-group {!! $errors->has('judul') ? 'has-error' : '' !!}">
	{!! Form::label('judul', 'Judul', ['class'=>'col-xs-12 col-sm-3 control-label no-padding-right']) !!} 
	<div class="col-xs-12 col-sm-5">
		<span class="block input-icon input-icon-right">
			{!! Form::text('judul', null, ['class'=>'width-100']) !!} 
		</span>
	</div>
	{!! $errors->first('judul', '<div class="help-block col-xs-12 col-sm-reset inline">:message</div>') !!} 
</div> <!-- judul -->

<div class="form-group {!! $errors->has('deskripsi') ? 'has-error' : '' !!}">
	{!! Form::label('deskripsi', 'Deskripsi', ['class'=>'col-xs-12 col-sm-3 control-label no-padding-right']) !!} 
	<div class="col-xs-12 col-sm-5">
		<span class="block input-icon input-icon-right">
			{!! Form::textarea('deskripsi', null, ['class'=>'width-100', 'rows'=>'3']) !!} 
		</span>
	</div>
	{!! $errors->first('deskripsi', '<div class="help-block col-xs-12 col-sm-reset inline">:message</div>') !!} 
</div> <!-- deskripsi -->