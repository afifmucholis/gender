<div class="form-group row {!! $errors->has('program') ? 'has-error' : '' !!}"> 
	{!! Form::label('program', 'Program', ['class'=>'col-xs-12 col-sm-3 control-label no-padding-right']) !!} 
	<div class="col-xs-12 col-sm-5">
		<span class="block input-icon input-icon-right">
			{!! Form::textarea('program', null, ['class'=>'form-control', 'rows'=>'3']) !!} 
		</span>
	</div>
	{!! $errors->first('program', '<div class="help-block col-xs-12 col-sm-reset inline">:message</div>') !!} 
</div> <!-- end program -->

<div class="form-group row {!! $errors->has('komponen') ? 'has-error' : '' !!}"> 
{!! Form::label('komponen', 'Komponen', ['class'=>'col-xs-12 col-sm-3 control-label no-padding-right']) !!} 
<div class="col-xs-12 col-sm-5">
		<span class="block input-icon input-icon-right">
{!! Form::textarea('komponen', null, ['class'=>'form-control', 'rows'=>'3']) !!} 
</span>
	</div>
	{!! $errors->first('komponen', '<p class="help-block col-xs-12 col-sm-reset inline">:message</p>') !!} 
</div> <!-- end komponen -->

<div class="form-group row {!! $errors->has('opd') ? 'has-error' : '' !!}"> 
{!! Form::label('opd', 'opd', ['class'=>'col-xs-12 col-sm-3 control-label no-padding-right']) !!} 
<div class="col-xs-12 col-sm-5">
		<span class="block input-icon input-icon-right">
{!! Form::textarea('opd', null, ['class'=>'form-control', 'rows'=>'3']) !!} 
</span>
	</div>
	{!! $errors->first('opd', '<p class="help-block col-xs-12 col-sm-reset inline">:message</p>') !!} 
</div> <!-- end opd -->

<div class="form-group row {!! $errors->has('program') ? 'has-error' : '' !!}"> 
{!! Form::label('program', 'program', ['class'=>'col-xs-12 col-sm-3 control-label no-padding-right']) !!} 
<div class="col-xs-12 col-sm-5">
		<span class="block input-icon input-icon-right">
{!! Form::textarea('program', null, ['class'=>'form-control', 'rows'=>'3']) !!} 
</span>
	</div>
	{!! $errors->first('program', '<p class="help-block col-xs-12 col-sm-reset inline">:message</p>') !!} 
</div> <!-- end program -->

<div class="form-group row {!! $errors->has('kegiatan') ? 'has-error' : '' !!}"> 
{!! Form::label('kegiatan', 'kegiatan', ['class'=>'col-xs-12 col-sm-3 control-label no-padding-right']) !!} 
<div class="col-xs-12 col-sm-5">
		<span class="block input-icon input-icon-right">
{!! Form::textarea('kegiatan', null, ['class'=>'form-control', 'rows'=>'3']) !!} 
</span>
	</div>
	{!! $errors->first('kegiatan', '<p class="help-block col-xs-12 col-sm-reset inline">:message</p>') !!} 
</div> <!-- end kegiatan -->

<div class="form-group row {!! $errors->has('indikator_kinerja') ? 'has-error' : '' !!}"> 
{!! Form::label('indikator_kinerja', 'indikator_kinerja', ['class'=>'col-xs-12 col-sm-3 control-label no-padding-right']) !!} 
<div class="col-xs-12 col-sm-5">
		<span class="block input-icon input-icon-right">
{!! Form::textarea('indikator_kinerja', null, ['class'=>'form-control', 'rows'=>'3']) !!} 
</span>
	</div>
	{!! $errors->first('indikator_kinerja', '<p class="help-block col-xs-12 col-sm-reset inline">:message</p>') !!} 
</div> <!-- end indikator_kinerja -->

<div class="form-group row {!! $errors->has('latar_belakang') ? 'has-error' : '' !!}"> 
{!! Form::label('latar_belakang', 'latar_belakang', ['class'=>'col-xs-12 col-sm-3 control-label no-padding-right']) !!} 
<div class="col-xs-12 col-sm-5">
		<span class="block input-icon input-icon-right">
{!! Form::textarea('latar_belakang', null, ['class'=>'form-control', 'rows'=>'3']) !!} 
</span>
	</div>
	{!! $errors->first('latar_belakang', '<p class="help-block col-xs-12 col-sm-reset inline">:message</p>') !!} 
</div> <!-- end latar_belakang -->

<div class="form-group row {!! $errors->has('gambaran_umum') ? 'has-error' : '' !!}"> 
{!! Form::label('gambaran_umum', 'gambaran_umum', ['class'=>'col-xs-12 col-sm-3 control-label no-padding-right']) !!} 
<div class="col-xs-12 col-sm-5">
		<span class="block input-icon input-icon-right">
{!! Form::textarea('gambaran_umum', null, ['class'=>'form-control', 'rows'=>'3']) !!} 
</span>
	</div>
	{!! $errors->first('gambaran_umum', '<p class="help-block col-xs-12 col-sm-reset inline">:message</p>') !!} 
</div> <!-- end gambaran_umum -->

<div class="form-group row {!! $errors->has('penerima_manfaat') ? 'has-error' : '' !!}"> 
{!! Form::label('penerima_manfaat', 'penerima_manfaat', ['class'=>'col-xs-12 col-sm-3 control-label no-padding-right']) !!} 
<div class="col-xs-12 col-sm-5">
		<span class="block input-icon input-icon-right">
{!! Form::textarea('penerima_manfaat', null, ['class'=>'form-control', 'rows'=>'3']) !!} 
</span>
	</div>
	{!! $errors->first('penerima_manfaat', '<p class="help-block col-xs-12 col-sm-reset inline">:message</p>') !!} 
</div> <!-- end penerima_manfaat -->

<div class="form-group row {!! $errors->has('maksud') ? 'has-error' : '' !!}"> 
{!! Form::label('maksud', 'maksud', ['class'=>'col-xs-12 col-sm-3 control-label no-padding-right']) !!} 
<div class="col-xs-12 col-sm-5">
		<span class="block input-icon input-icon-right">
{!! Form::textarea('maksud', null, ['class'=>'form-control', 'rows'=>'3']) !!} 
</span>
	</div>
	{!! $errors->first('maksud', '<p class="help-block col-xs-12 col-sm-reset inline">:message</p>') !!} 
</div> <!-- end maksud -->

<div class="form-group row {!! $errors->has('cara_pelaksanaan') ? 'has-error' : '' !!}"> 
{!! Form::label('cara_pelaksanaan', 'cara_pelaksanaan', ['class'=>'col-xs-12 col-sm-3 control-label no-padding-right']) !!} 
<div class="col-xs-12 col-sm-5">
		<span class="block input-icon input-icon-right">
{!! Form::textarea('cara_pelaksanaan', null, ['class'=>'form-control', 'rows'=>'3']) !!} 
</span>
	</div>
	{!! $errors->first('cara_pelaksanaan', '<p class="help-block col-xs-12 col-sm-reset inline">:message</p>') !!} 
</div> <!-- end cara_pelaksanaan -->

<div class="form-group row {!! $errors->has('lokasi') ? 'has-error' : '' !!}"> 
{!! Form::label('lokasi', 'lokasi', ['class'=>'col-xs-12 col-sm-3 control-label no-padding-right']) !!} 
<div class="col-xs-12 col-sm-5">
		<span class="block input-icon input-icon-right">
{!! Form::textarea('lokasi', null, ['class'=>'form-control', 'rows'=>'3']) !!} 
</span>
	</div>
	{!! $errors->first('lokasi', '<p class="help-block col-xs-12 col-sm-reset inline">:message</p>') !!} 
</div> <!-- end lokasi -->

<div class="form-group row {!! $errors->has('pelaksana') ? 'has-error' : '' !!}"> 
{!! Form::label('pelaksana', 'pelaksana', ['class'=>'col-xs-12 col-sm-3 control-label no-padding-right']) !!} 
<div class="col-xs-12 col-sm-5">
		<span class="block input-icon input-icon-right">
{!! Form::textarea('pelaksana', null, ['class'=>'form-control', 'rows'=>'3']) !!} 
</span>
	</div>
	{!! $errors->first('pelaksana', '<p class="help-block col-xs-12 col-sm-reset inline">:message</p>') !!} 
</div> <!-- end pelaksana -->

<div class="form-group row {!! $errors->has('batas') ? 'has-error' : '' !!}"> 
{!! Form::label('batas', 'batas', ['class'=>'col-xs-12 col-sm-3 control-label no-padding-right']) !!} 
<div class="col-xs-12 col-sm-5">
		<span class="block input-icon input-icon-right">
{!! Form::textarea('batas', null, ['class'=>'form-control', 'rows'=>'3']) !!} 
</span>
	</div>
	{!! $errors->first('batas', '<p class="help-block col-xs-12 col-sm-reset inline">:message</p>') !!} 
</div> <!-- end batas -->

<div class="form-group row {!! $errors->has('jadwal') ? 'has-error' : '' !!}"> 
{!! Form::label('jadwal', 'jadwal', ['class'=>'col-xs-12 col-sm-3 control-label no-padding-right']) !!} 
<div class="col-xs-12 col-sm-5">
		<span class="block input-icon input-icon-right">
{!! Form::textarea('jadwal', null, ['class'=>'form-control', 'rows'=>'3']) !!} 
</span>
	</div>
	{!! $errors->first('jadwal', '<p class="help-block col-xs-12 col-sm-reset inline">:message</p>') !!} 
</div> <!-- end jadwal -->

<div class="form-group row {!! $errors->has('biaya') ? 'has-error' : '' !!}"> 
{!! Form::label('biaya', 'biaya', ['class'=>'col-xs-12 col-sm-3 control-label no-padding-right']) !!} 
<div class="col-xs-12 col-sm-5">
		<span class="block input-icon input-icon-right">
{!! Form::textarea('biaya', null, ['class'=>'form-control', 'rows'=>'3']) !!} 
</span>
	</div>
	{!! $errors->first('biaya', '<p class="help-block col-xs-12 col-sm-reset inline">:message</p>') !!} 
</div> <!-- end biaya -->