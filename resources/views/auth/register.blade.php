@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Register</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Nama</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('nip') ? ' has-error' : '' }}">
                            <label for="nip" class="col-md-4 control-label">NIP</label>

                            <div class="col-md-6">
                                <input id="nip" type="text" class="form-control" name="nip" value="{{ old('nip') }}" required autofocus>

                                @if ($errors->has('nip'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('nip') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('jabatan') ? ' has-error' : '' }}">
                            <label for="jabatan" class="col-md-4 control-label">Jabatan</label>

                            <div class="col-md-6">
                                <input id="jabatan" type="text" class="form-control" name="jabatan" value="{{ old('jabatan') }}" required autofocus>

                                @if ($errors->has('jabatan'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('jabatan') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('hp') ? ' has-error' : '' }}">
                            <label for="hp" class="col-md-4 control-label">No HP</label>

                            <div class="col-md-6">
                                <input id="hp" type="number" class="form-control" name="hp" value="{{ old('hp') }}" required autofocus>

                                @if ($errors->has('hp'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('hp') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('opd') ? ' has-error' : '' }}">
                            <label for="opd" class="col-md-4 control-label">OPD</label>

                            <div class="col-md-6">
                                <input id="opd" type="text" class="form-control" name="opd" value="{{ old('opd') }}" required autofocus>

                                @if ($errors->has('opd'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('opd') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('bidang') ? ' has-error' : '' }}">
                            <label for="bidang" class="col-md-4 control-label">Bidang</label>

                            <div class="col-md-6">
                                <input id="bidang" type="text" class="form-control" name="bidang" value="{{ old('bidang') }}" required autofocus>

                                @if ($errors->has('bidang'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('bidang') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('sub_bidang') ? ' has-error' : '' }}">
                            <label for="sub_bidang" class="col-md-4 control-label">Sub Bidang</label>

                            <div class="col-md-6">
                                <input id="sub_bidang" type="text" class="form-control" name="sub_bidang" value="{{ old('sub_bidang') }}" required autofocus>

                                @if ($errors->has('sub_bidang'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('sub_bidang') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email_lembaga') ? ' has-error' : '' }}">
                            <label for="email_lembaga" class="col-md-4 control-label">E-Mail Lembaga</label>

                            <div class="col-md-6">
                                <input id="email_lembaga" type="email" class="form-control" name="email_lembaga" value="{{ old('email_lembaga') }}" required>

                                @if ($errors->has('email_lembaga'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email_lembaga') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('telp_kantor') ? ' has-error' : '' }}">
                            <label for="telp_kantor" class="col-md-4 control-label">Telpon Kantor</label>

                            <div class="col-md-6">
                                <input id="telp_kantor" type="text" class="form-control" name="telp_kantor" value="{{ old('telp_kantor') }}" required autofocus>

                                @if ($errors->has('telp_kantor'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('telp_kantor') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
