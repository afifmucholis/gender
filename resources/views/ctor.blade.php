@extends('layouts.user')

@section('content')
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs breadcrumbs-fixed" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>

				<li>
					<a href="#">Forms</a>
				</li>
				<li class="active">Wizard &amp; Validation</li>
			</ul><!-- /.breadcrumb -->

			<div class="nav-search" id="nav-search">
				<form class="form-search">
					<span class="input-icon">
						<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
						<i class="ace-icon fa fa-search nav-search-icon"></i>
					</span>
				</form>
			</div><!-- /.nav-search -->
		</div>

		<div class="page-content">

			<div class="page-header">
				<h1>
					Create New Project
				</h1>
			</div><!-- /.page-header -->

			<div class="row">
				<div class="col-xs-12">
					<div class="widget-box">
						<div class="widget-header widget-header-blue widget-header-flat">
							<h4 class="widget-title lighter">Form</h4>
						</div>

						<div class="widget-body">
							<div class="widget-main">
								<div id="fuelux-wizard-container">
									<div>
										<ul class="steps">
											<li data-step="1">
												<span class="step">1</span>
												<span class="title">Project</span>
											</li>

											<li data-step="2">
												<span class="step">2</span>
												<span class="title">GAP</span>
											</li>

											<li data-step="3">
												<span class="step">3</span>
												<span class="title">GBS</span>
											</li>

											<li data-step="4" class="active">
												<span class="step">4</span>
												<span class="title">TOR</span>
											</li>
										</ul>
									</div>

									<hr /> <!-- step -->

									<div class="step-content pos-rel">
										<div class="step-pane active" data-step="1">
											{!! Form::open(['route' => 'tors.store', 'class'=>'form-horizontal'])!!} 
											{{ Form::text('project_id', $id) }}
											@include('_torForm')
										</div>

									</div>
								</div>

								<hr />
								<div class="wizard-actions">
									<button type="submit" class="btn btn-success btn-next" data-last="Finish">
										Next
										<i class="ace-icon fa fa-arrow-right icon-on-right"></i>
									</button>
								</div>
											{!! Form::close() !!} 
							</div><!-- /.widget-main -->
						</div><!-- /.widget-body -->
					</div>							
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.page-content -->

		@endsection