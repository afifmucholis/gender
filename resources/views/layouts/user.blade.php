<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>{{ config('app.name', 'Gender') }}</title>
		<meta name="description" content="overview &amp; stats" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<link rel="stylesheet" href="{{ asset('1/bootstrap.min.css') }}" />
		<link rel="stylesheet" href="{{ asset('1/font-awesome.min.css') }}" />
		<link rel="stylesheet" href="{{ asset('1/fonts.googleapis.com.css') }}" />
		<link rel="stylesheet" href="{{ asset('1/ace.min.css') }}" />
		<script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
	</head>

	<body class="no-skin">
		<div id="navbar" class="navbar navbar-default navbar-fixed-top">
			<div class="navbar-container container" id="navbar-container">
				<button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
					<span class="sr-only">Toggle sidebar</span>

					<span class="icon-bar"></span>

					<span class="icon-bar"></span>

					<span class="icon-bar"></span>
				</button>

				<div class="navbar-header pull-left">
					<a href="index.html" class="navbar-brand">
						<small>
							<i class="fa fa-leaf"></i>
							Gendar
						</small>
					</a>
				</div>

				<div class="navbar-buttons navbar-header pull-right" role="navigation">
					<ul class="nav ace-nav">

						<li class="light-blue">
							<a data-toggle="dropdown" href="#" class="dropdown-toggle">
								<img class="nav-user-photo" src="{{ asset('1/user.jpg') }}" alt="Jason's Photo" />
								<span class="user-info">
									<small>Welcome,</small>
									Jason
								</span>

								<i class="ace-icon fa fa-caret-down"></i>
							</a>

							<ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
								<li>
									<a href="#">
										<i class="ace-icon fa fa-power-off"></i>
										Logout
									</a>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</div><!-- /.navbar-container -->
		</div>

		<div class="main-container container" id="main-container">
			<div id="sidebar" class="sidebar responsive sidebar-fixed">
				<div class="sidebar-shortcuts " id="sidebar-shortcuts">
					<div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
						<button class="btn btn-success">
							<i class="ace-icon fa fa-signal"></i>
						</button>

						<button class="btn btn-info">
							<i class="ace-icon fa fa-pencil"></i>
						</button>

						<button class="btn btn-warning">
							<i class="ace-icon fa fa-users"></i>
						</button>

						<button class="btn btn-danger">
							<i class="ace-icon fa fa-cogs"></i>
						</button>
					</div>

					<div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
						<span class="btn btn-success"></span>

						<span class="btn btn-info"></span>

						<span class="btn btn-warning"></span>

						<span class="btn btn-danger"></span>
					</div>
				</div><!-- /.sidebar-shortcuts -->

				<ul class="nav nav-list">
					<li>
						<a href="{{ url('/project/create') }}">
							<i class="menu-icon fa fa-tachometer"></i>
							<span class="menu-text"> Create Projects </span>
						</a>

						<b class="arrow"></b>
					</li>

					<li>
						<a href="{{ url('/project') }}">
							<i class="menu-icon fa fa-list"></i>
							<span class="menu-text"> All Projects </span>
						</a>

						<b class="arrow"></b>
					</li>

					<li>
						<a href="{{ url('/gaps') }}">
							<i class="menu-icon fa fa-list"></i>
							<span class="menu-text"> GAP </span>
						</a>

						<b class="arrow"></b>
					</li>
					<li>
						<a href="{{ url('/gbss') }}">
							<i class="menu-icon fa fa-list"></i>
							<span class="menu-text"> GBS </span>
						</a>

						<b class="arrow"></b>
					</li>
					<li>
						<a href="{{ url('/tors') }}">
							<i class="menu-icon fa fa-list"></i>
							<span class="menu-text"> TOR </span>
						</a>

						<b class="arrow"></b>
					</li>
					<li>
						<a href="{{ url('/tors') }}">
							<i class="menu-icon fa fa-list"></i>
							<span class="menu-text"> TOR </span>
						</a>

						<b class="arrow"></b>
					</li>




				<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
					<i class="ace-icon fa fa-angle-double-left" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
				</div>

			</div>
			@yield('content')
			

			<div class="footer">
				<div class="footer-inner">
					<div class="footer-content">
						<span class="bigger-120">
							<span class="blue bolder">Gender</span>
							&copy; 2017
						</span>
					</div>
				</div>
			</div>

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

		<!-- basic scripts -->

		<!--[if !IE]> -->
		<script src="assets/js/jquery.2.1.1.min.js"></script>

		<script type="text/javascript">
			window.jQuery || document.write("<script src='{{ asset('1/jquery.min.js') }}'>"+"<"+"/script>");
		</script>
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='{{ asset('1/jquery.mobile.custom.min.js') }}'>"+"<"+"/script>");
		</script>
		<script src="{{ asset('1/bootstrap.min.js') }}"></script>
		<script src="{{ asset('1/jquery.dataTables.min.js') }}"></script>
		<script src="{{ asset('1/jquery.dataTables.bootstrap.min.js') }}"></script>
		<script src="{{ asset('1/dataTables.tableTools.min.js') }}"></script>
		<script src="{{ asset('1/dataTables.colVis.min.js') }}"></script>
		<script src="{{ asset('1/ace-elements.min.js') }}"></script>
		<script src="{{ asset('1/ace.min.js') }}"></script>
		@yield('js')
		
	</body>
</html>
