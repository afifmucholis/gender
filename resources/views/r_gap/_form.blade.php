<div class="form-group {!! $errors->has('tahun') ? 'has-error' : '' !!}"> 
{!! Form::label('title', 'Tahun') !!} 
{!! Form::text('tahun', null, ['class'=>'form-control']) !!} 
{!! $errors->first('tahun', '<p class="help-block">:message</p>') !!} 
</div>
<div class="form-group {!! $errors->has('no') ? 'has-error' : '' !!}"> 
{!! Form::label('title', 'No Langkah') !!} 
{!! Form::text('no', null, ['class'=>'form-control']) !!} 
{!! $errors->first('no', '<p class="help-block">:message</p>') !!} 
</div>
<div class="form-group {!! $errors->has('nama') ? 'has-error' : '' !!}"> 
{!! Form::label('title', 'Nama') !!} 
{!! Form::text('nama', null, ['class'=>'form-control']) !!} 
{!! $errors->first('nama', '<p class="help-block">:message</p>') !!} 
</div>
<div class="form-group {!! $errors->has('deskripsi') ? 'has-error' : '' !!}"> 
{!! Form::label('title', 'Deskripsi') !!} 
{!! Form::text('deskripsi', null, ['class'=>'form-control']) !!} 
{!! $errors->first('deskripsi', '<p class="help-block">:message</p>') !!} 
</div>
{!! Form::submit(isset($model) ? 'Update' : 'Save', ['class'=>'btn btn-primary'] ) !!}