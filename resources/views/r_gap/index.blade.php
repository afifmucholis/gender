@extends('layouts.app')

@section('content')
<div class="container"> 
	<div class="row"> 
		<div class="col-md-12"> 
			<h3>Referensi GAP</h3> 
			<table class="table table-hover"> 
				<thead> 
					<tr> 
						<td>No Langkah</td> 
						<td>Tahun</td>
						<td>Nama</td> 
						<td>Deskripsi</td> 
					</tr> 
				</thead> 
				<tbody> 
					@foreach($r_gap as $gap) 
					<tr> 
						<td>{{ $gap->no }}</td> 
						<td>{{ $gap->tahun }}</td> 
						<td>{{ $gap->nama }}</td> 
						<td>{{ $gap->deskripsi }}</td> 
					</tr>
					@endforeach 
				</tbody> 
			</table> 
		</div> 
	</div> 
</div> 
@endsection