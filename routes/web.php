<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

// Route::get('/home', 'HomeController@index');
Route::group(['middleware' => 'web'], function () { 
	Route::auth(); 
	Route::get('/home', 'HomeController@index'); 
	// Route::resource('r_gaps', 'r_gapsController'); 
	Route::resource('gaps', 'GapsController'); 
	Route::resource('gbss', 'GbssController'); 
	Route::resource('tors', 'TorsController'); 
	Route::resource('project', 'ProjectsController'); 
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
